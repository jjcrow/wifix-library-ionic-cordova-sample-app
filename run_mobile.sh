#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: $0 [android|ios]"
  exit 1
fi

# Function to run a command with retries
function run_with_retry {
  local cmd="$1"
  local retries=2
  local count=0

  while [ $count -lt $retries ]; do
    $cmd
    result=$?
    if [ $result -eq 0 ]; then
      break
    fi
    count=$((count+1))
    echo "Command failed, retrying... (Attempt $count of $retries)"
  done

  if [ $result -ne 0 ]; then
    echo "Command failed after $retries attempts. Exiting with error."
    exit 1
  fi
}

# Remove and add the Cordova plugin with retries
if [ "$2" == "--fresh-install" ]; then
  echo "Removing and adding plugin"
  run_with_retry "ionic cordova plugin rm org.smartsense.wifixscan.coreplugin --verbose"
  ionic cordova plugin add ./../../Library/WifixScan --link
fi

# Run the Cordova Android build with live reload and verbose output
if [ "$1" == "android" ]; then
  ionic cordova prepare $1 -l --verbose --external
  ionic cordova build $1 -l --verbose
else
  ionic cordova prepare $1 -l --verbose --external
  ionic cordova build $1 --device -l --verbose --external
fi
