# Wifix Scan CorePlugin Ionic Cordova SAmple app

this app is a sample on how to use the wifix scan plugin in a ionic cordova app

## Instructions

just add the platforms doing:

`ionic cordova platform add android`

`ionic cordova platform add ios`

then you can use the script in the root folder `run_mobile.sh`, and ou can build doing the following:

`./run_mobile.sh android`

or

`./run_mobile.sh ios`

notice that for ios, you will need to have a Development Certificate and a provisioning profile.
