import { Component } from '@angular/core';
import { WifixScanCorePlugin } from 'awesome-cordova-plugins-example/ngx';
import { ToastController } from '@ionic/angular';
import { ChangeDetectorRef, NgZone } from '@angular/core';

declare var window: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  label: String = '';
  shouldShowSupport: Boolean = false;
  supportManager: any = null;
  remoteAssistanceButtonTitle = 'Start Remote Assitance';
  remoteAssitanceUrl = '';
  remoteAssistanceCloseCallback?: () => void;

  constructor(
    private toastController: ToastController,
    private cdr: ChangeDetectorRef,
    private zone: NgZone
  ) {}

  async testLibrary(type: string) {
    this.shouldShowSupport = false;
    this.label = `** Waiting for value on test type ${type} ** `;
    console.log('Executing custom plugin');

    console.log('The plugin definition is:');
    console.log(window.cordova.plugins);
    let p = new window.cordova.plugins.WifixScan();

    console.log('plugin is: ');
    console.log(p);

    if (type == 'full') {
      p.doFullScan(
        (supportManager: any) => {
          console.log('===> The plugin result was: ');
          console.log(supportManager);
          this.label = `:D Successful Full Scan With Code: ${
            supportManager.getCurrentScanData().scanCode
          }`;

          this.success(supportManager);
        },
        (error: any) => {
          this.label = `:( Error on Full Scan  :( `;
          console.log('XXX> error from ');
          console.log(error);
        },
        '1234567890',
        '0a9d9c7866c545458e31e3a'
      );
    } else {
      p.doQuickScan(
        (supportManager: any) => {
          console.log('===> The plugin result was: ');
          console.log(supportManager);
          this.label = `:D Successful Support Scan With Code: ${
            supportManager.getCurrentScanData().scanCode
          }`;

          this.success(supportManager);
        },
        (error: any) => {
          console.log('XXX> error from ');
          console.log(error);
          this.label = `:( Error on Quick (Support) Scan  :( `;
        },
        '1234567890',
        '0a9d9c7866c545458e31e3a'
      );
    }
  }

  success(data: any) {
    this.zone.run(() => {
      this.supportManager = data;
      this.shouldShowSupport = true;
      console.log(
        `===> Executing succesful completion with should Support: ${this.shouldShowSupport} and support manager: ${this.supportManager}`
      );
    });
  }

  async runScanTest(type: 'signal' | 'wifis' | 'devices' | 'speed') {
    let testResult: any = null;
    switch (type) {
      case 'signal':
        testResult = await this.supportManager.runWifiSignalTest();
        break;
      case 'wifis':
        testResult =
          await this.supportManager.runCurrentWifiAndNearestWifiNetworksTest();
        break;
      case 'devices':
        testResult =
          await this.supportManager.runCurrentNetworkInfoAndDevicesConnectedTest();
        break;
      case 'speed':
        testResult = await this.supportManager.runSpeedTest();
    }

    console.log(`===> Test ${type} result:`);
    console.log(testResult);
    this.presentToast(
      `TEST ${type} ended with result:\n${JSON.stringify(testResult)}`,
      'middle'
    );
  }

  async takePicture() {
    let result = await this.supportManager.takeAndUploadPicture();
    console.log('====> Picture result: ');
    console.log(result);
  }

  async startRemoteAssitance() {
    if (this.remoteAssistanceCloseCallback) {
      this.remoteAssistanceCloseCallback();
      this.remoteAssistanceButtonTitle = 'Start Remote Assistance';
      this.remoteAssitanceUrl = '';
      this.remoteAssistanceCloseCallback = undefined;
      return;
    }

    this.remoteAssistanceButtonTitle = 'Start Remote Assistance';
    this.remoteAssitanceUrl = '';

    this.supportManager.startRemoteAssistance(
      (result: {
        remoteAssistanceURL: string;
        closeRemoteAssistance: (() => void) | undefined;
      }) => {
        if (result.remoteAssistanceURL) {
          this.zone.run(() => {
            this.remoteAssitanceUrl = result.remoteAssistanceURL;
          });
        }
        if (result.closeRemoteAssistance) {
          this.zone.run(() => {
            this.remoteAssistanceCloseCallback = result.closeRemoteAssistance;
            this.remoteAssistanceButtonTitle = 'Stop Remote Assistance';
          });
        }
      },
      (error: string) => {
        console.log(`===> Error while requesting remote assistance: ${error}`);
      },
      () => {
        console.log('===> Assistance was remotely closed!');
      }
    );
  }

  async presentToast(message: string, position: 'top' | 'middle' | 'bottom') {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      position: position,
    });

    await toast.present();
  }

  resetToMain() {
    this.shouldShowSupport = false;
    this.supportManager = null;
    this.label = '';
  }
}
